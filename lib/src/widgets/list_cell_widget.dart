import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note_markdown/src/screens/edit_screen.dart';

class ListCellWidget extends StatelessWidget {
  final Map<String, dynamic> use;
  ListCellWidget(this.use);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          CupertinoPageRoute(
            builder: (context) => EditScreen(
              id: use['id'],
              memo: use['memo'],
            ),
          ),
        );
      },
      child: Card(
        child: ListTile(
          title: Text(use['memo']),
          subtitle: Text('これはメモアプリの中のタイトルです。'),
        ),
      ),
    );
  }
}
