import 'package:flutter/material.dart';

class CustomAlertWidget extends StatelessWidget {
  final String message;
  CustomAlertWidget({this.message});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(message),
      content: Text('保存に$messageしました'),
      actions: <Widget>[
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('OK'),
        ),
      ],
    );
  }
}
