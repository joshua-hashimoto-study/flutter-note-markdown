import 'package:flutter/material.dart';
import 'package:note_markdown/src/screens/home_screen.dart';

class InitializedApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Markdown Dome',
      theme: ThemeData(
        scaffoldBackgroundColor: Color(0xFF0A0E21),
        primaryColor: Color(0xFF4C4F5E),
        textTheme: TextTheme(
          title: TextStyle(
            color: Colors.white,
          ),
        ),
      ),
      home: HomeScreen(),
    );
  }
}
