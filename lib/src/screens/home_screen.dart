import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:note_markdown/src/screens/edit_screen.dart';
import 'package:note_markdown/src/store/db_provider.dart';
import 'package:note_markdown/src/widgets/list_cell_widget.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<List<Map<String, dynamic>>> _getMemo() async {
    final db = await DBProvider.db.database;
    final res = await db.query("Memo");
    return res;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'ホーム',
          style: Theme.of(context).textTheme.title,
        ),
      ),
      body: FutureBuilder(
        future: _getMemo(),
        builder: (BuildContext context, AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
          if (!snapshot.hasData) {
            return Container();
          }
          List<Map<String, dynamic>> memos = snapshot.data;
          return ListView.builder(
            itemCount: memos.length,
            itemBuilder: (BuildContext context, int index) {
              return ListCellWidget(memos[index]);
            },
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.teal.shade300,
        onPressed: () {
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (content) => EditScreen(),
            ),
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
