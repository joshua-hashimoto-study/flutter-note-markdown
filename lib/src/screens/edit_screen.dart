import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:note_markdown/src/store/db_provider.dart';
import 'package:note_markdown/src/widgets/custom_alert_widget.dart';

const Color mainColor = Color(0xFF4C4F5E);

class EditScreen extends StatefulWidget {
  final int id;
  final String memo;

  EditScreen({this.id, this.memo});

  @override
  _EditScreenState createState() => _EditScreenState();
}

class _EditScreenState extends State<EditScreen> with SingleTickerProviderStateMixin {
  TabController _tabController;
  TextEditingController _textEditingController = TextEditingController();

  String editText = '';

  List<Widget> _tabs = [
    Tab(
      child: Text('preview'),
    ),
    Tab(
      child: Text('editing'),
    ),
  ];

  Future<void> _saveMemo(String text) async {
    final db = await DBProvider.db.database;
    if (widget.id == null) {
      final res = await db.rawInsert("INSERT Into Memo (memo)"
          " VALUES ('$text')");
      return res;
    }
    Map<String, dynamic> memo = Map();
    memo['memo'] = text;
    memo['id'] = widget.id;
    final res = await db.update("Memo", memo, where: "id = ?", whereArgs: [widget.id]);
    return res;
  }

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
      length: _tabs.length,
      vsync: this,
    );
    editText = widget.memo ?? '';
    _textEditingController.text = widget.memo ?? '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('編集'),
        bottom: TabBar(
          controller: _tabController,
          tabs: _tabs,
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () async {
              try {
                await _saveMemo(editText);
                await showDialog(
                    context: context,
                    builder: (_) {
                      return CustomAlertWidget(
                        message: '成功',
                      );
                    });
                Navigator.pop(context);
              } catch (err) {
                print(err);
                await showDialog(
                    context: context,
                    builder: (_) {
                      return CustomAlertWidget(
                        message: '失敗',
                      );
                    });
              }
            },
            child: Icon(Icons.save),
          ),
        ],
      ),
      body: TabBarView(
        controller: _tabController,
        children: [
          _previewPage(),
          _editPage(),
        ],
      ),
    );
  }

  Widget _previewPage() {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Markdown(
        data: editText,
        styleSheet: MarkdownStyleSheet(
          h1: TextStyle(
            color: mainColor,
            fontSize: 36.0,
            height: 2.0,
          ),
          h2: TextStyle(
            color: mainColor,
            fontSize: 24.0,
            height: 3.0,
          ),
          em: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.red,
          ),
        ),
      ),
    );
  }

  Widget _editPage() {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: TextField(
        controller: _textEditingController,
        maxLines: null,
        keyboardType: TextInputType.multiline,
        style: TextStyle(
          color: mainColor,
        ),
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: '入力してください。',
        ),
        onChanged: (String text) {
          setState(() {
            editText = text;
          });
        },
      ),
    );
  }
}
